using System;
using System.Device.Adc;
using System.Device.Gpio;
using System.Device.I2c;
using System.Device.Spi;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using Iot.Device.Buzzer;
using Iot.Device.Ssd13xx;
using Iot.Device.Ssd1351;
using nanoFramework.Hardware.Esp32;
using nanoFramework.Networking;

namespace NanoTest
{
    public class Program
    {
        private static Ssd1306 _display;
        private static Iot.Device.SpiSsd13xx.Ssd1306 _greenDisplay;
        private static Ssd1351 _colorDisplay;

        private static AdcChannel _vp;
        private static AdcChannel _vn;
        private static AdcChannel _vb;

        public static void Main()
        {
            Debug.WriteLine("Hello from nanoFramework!");

            // while (true)
            // {
            //     Debug.Write("1");
            //     Thread.Sleep(2500);
            // }

            Debug.WriteLine($"Time is {DateTime.UtcNow}");

            Configuration.SetPinFunction(13, DeviceFunction.I2C1_DATA);
            Configuration.SetPinFunction(16, DeviceFunction.I2C1_CLOCK);
            Configuration.SetPinFunction(36, DeviceFunction.ADC1_CH0);
            Configuration.SetPinFunction(39, DeviceFunction.ADC1_CH3);
            Configuration.SetPinFunction(35, DeviceFunction.ADC1_CH7); // Vbatt
            Configuration.SetPinFunction(14, DeviceFunction.SPI1_CLOCK);
            Configuration.SetPinFunction(15, DeviceFunction.SPI1_MOSI);
            Configuration.SetPinFunction(32, DeviceFunction.SPI1_MISO);
            Configuration.SetPinFunction(33, DeviceFunction.PWM1);

            var controller = new AdcController();
            _vp = controller.OpenChannel(0);
            _vn = controller.OpenChannel(3);
            _vb = controller.OpenChannel(7);

            _greenDisplay = new Iot.Device.SpiSsd13xx.Ssd1306(SpiDevice.Create(new SpiConnectionSettings(1, 2)), 1, 0);
            _greenDisplay.Font = new BasicFont();
            _greenDisplay.ClearScreen();

            _colorDisplay = new Ssd1351(SpiDevice.Create(new SpiConnectionSettings(1, 5)), 4, 3);
            _colorDisplay.ResetDisplay();
            _colorDisplay.Unlock();
            _colorDisplay.SetDisplayOn();
            _colorDisplay.SetDisplayAllOn();

            _display = new Ssd1306(I2cDevice.Create(new I2cConnectionSettings(1, Ssd1306.DefaultI2cAddress)), Ssd13xx.DisplayResolution.OLED128x32);
            _display.Font = new BasicFont();

            UpdateDisplay("Starting...");

            // var controller = new GpioController();
            // var led = controller.OpenPin(2, PinMode.Output);

            // led.Write(PinValue.Low);

            // while (true)
            // {
            //     led.Toggle();
            //     Thread.Sleep(125);
            //     led.Toggle();
            //     Thread.Sleep(125);
            //     led.Toggle();
            //     Thread.Sleep(125);
            //     led.Toggle();
            //     Thread.Sleep(525);
            // }

            // if (!NetworkHelper.SetupAndConnectNetwork(requiresDateTime: false))
            // {
            //     Debug.WriteLine($"Unable to setup netowrking: {NetworkHelper.Status}");
            // }

            UpdateDisplay("Netted");

            // Debug.WriteLine($"Time is {DateTime.UtcNow}");

            // try 
            // {
            //     using (var buzzer = new Buzzer(33))
            //     {
            //         for (int i = 0; i < 10; i++)
            //         {
            //             buzzer.StartPlaying(440 * (i % 2 + 1));
            //             Thread.Sleep(1000);
            //             buzzer.StopPlaying();
            //             Thread.Sleep(1000);
            //         }
            //     }
            // }
            // catch (Exception ex)
            // {
            //     ShowException(ex);
            // }

            UpdateDisplay("Bipp");

            while (true)
            {
                UpdateDisplay();
                Thread.Sleep(1000 - DateTime.UtcNow.Millisecond);
            }

            Thread.Sleep(Timeout.Infinite);

            // Browse our samples repository: https://github.com/nanoframework/samples
            // Check our documentation online: https://docs.nanoframework.net/
            // Join our lively Discord community: https://discord.gg/gCyBu8T
        }

        private static void UpdateDisplay(string message = null)
        {
            if (!string.IsNullOrEmpty(message))
            {
                _display.ClearScreen();
                _display.DrawString(2, 0, message, 2);//large size 2 font
            }

            double p = _vp.ReadValue() * 3.3 / 4096;
            double n = _vn.ReadValue() * 3.3 / 4096;
            double b = _vn.ReadValue() * 3.3 / 4096;

            // _display.DrawString(0, 15, $"{DateTime.UtcNow:dd/MM/yyyy}", 1, true);//centered text
            _display.DrawString(0, 15, $"Vn:{n:N2}, Vp:{p:N2}", 1, true);//centered text
            // _display.DrawString(0, 23, $"Vb:{b:N2}", 1, true);//centered text
            _display.DrawString(0, 23, $"{DateTime.UtcNow:HH:mm:ss}", 1, true);//centered text
            _display.Display();

            try
            {
                _greenDisplay.DrawString(0, 0, $"{DateTime.UtcNow:dd/MM/yyyy}", 2, true);
                _greenDisplay.DrawString(0, 32, $"{DateTime.UtcNow:HH:mm:ss}", 2, true);
                _greenDisplay.Display();
            }
            catch (Exception ex)
            {
                ShowException(ex);
            }

            // try
            // {
            //     _colorDisplay.FillRect((DateTime.UtcNow.Second % 2) == 0 ? Color.Red : Color.Green, 10, 10, 107, 20);
            // }
            // catch (Exception ex)
            // {
            //     ShowException(ex);
            // }
            // _colorDisplay.Display();
        }

        private static void ShowException(Exception ex)
        {
            var msg = ex.ToString();
            while (msg.Length > 0)
            {
                var l = 0;
                while (msg.Length > 0 && l < 4)
                {
                    var line = msg.Substring(0, Math.Min(16, msg.Length));
                    msg = line.Length == msg.Length ? "" : msg.Substring(16);
                    _display.DrawString(0, l * 8 - 1, line.PadRight(16), 1, false);
                    l ++;
                }
                _display.Display();
                Thread.Sleep(2500);
            }
        }
    }
}
